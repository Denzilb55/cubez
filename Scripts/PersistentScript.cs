﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Facebook.MiniJSON;

#if UNITY_ANDROID
using StartApp;
#endif
public enum LoadMode {Sequential = 0, Category, LowestUncompleted, LowestLocked}

public class PersistentScript : MonoBehaviour {

	public GameObject hintCubeTemplate;
	public LoadMode loadMode = LoadMode.LowestUncompleted;
	private static PersistentScript instance = null;
	public int totalScore = 0;
	public int Level0 = 2;
	public int lastLoadedLevel;
	private Manager currentManager;

	public bool MusicEnabled
	{
		get
		{
			return songPlayer.allowedSongs != Songs.none;
		}
	}
	public bool SoundEnabled = true;
	
	public bool[] unlockedLevels;
	public int[] completedLevels;
	public SongPlayer songPlayer;
	
	private bool gameLevel = false;

	public bool LockGameControls = false;
	private bool quitting = false;

	public bool themeDark;
	public bool effectsOn = true;


	public static PersistentScript Instance {
		get { return instance; }
	}

	/*
	 * LEVEL MANAGEMENT
	 */
	public const int CATEGORY_COUNT = 3;
	public List<GameObject> levels = new List<GameObject>();
	public List<LevelScript> levelScripts = new List<LevelScript> ();
	public List<LevelScript> [] levelsCategorized = new List<LevelScript>[CATEGORY_COUNT];

	//private bool[] unlocked;
	private LevelScript _current_level;
	private Category _current_category;
	public LevelScript CurrentLevel {
		get { return _current_level;}
				private set{
					_current_category = value.category;
					CurrentIndexCat = FindIndexInCat(value);
					CurrentIndexUncat = FindIndexInUncat(value);
					_current_level = value;
				}
	}

	public int CurrentIndexCat { get; private set; }
	public int CurrentIndexUncat { get; private set; }

	public int unlockedIndex = 0;
	public List<LevelScript> recentlyUnlockedLevels = new List<LevelScript>();
	public LevelScript retryLevel;
	public bool highScore;
	public int victoryScore;

	private bool mustSave = false;



	const string AppId = "472265106241349";
	public const string ShareUrl = "http://www.facebook.com/dialog/feed";
	public const string cubezFree = "https://play.google.com/store/apps/details?id=com.MiniGames.CubeZ";

	#region FB.Init()
	public bool isInit = false;
	
	public void CallFBInit()
	{
		FB.Init(OnInitComplete, OnHideUnity);
	}
	
	private void OnInitComplete()
	{
		isInit = true;
	}
	
	private void OnHideUnity(bool isGameShown)
	{
		Debug.Log("Is game showing? " + isGameShown);
	}
	#endregion

	#region FB.Login() example
	
	public void CallFBLogin()
	{
		if (!FB.IsLoggedIn) {
			FB.Login ("email,publish_actions", LoginCallback);
		}
	}
	
	void LoginCallback(FBResult result)
	{
		if (!String.IsNullOrEmpty(result.Error)) {
	
		}
		else if (!FB.IsLoggedIn)
		{

		}
		else
		{
		//	lastResponse = "Login was successful!";
		}
	}
	
	private void CallFBLogout()
	{
		FB.Logout();
	}
	#endregion

	#region FB.Feed() example


	/*LabelAndTextField("To Id (optional): ", ref FeedToId);
	LabelAndTextField("Link (optional): ", ref FeedLink);
	LabelAndTextField("Link Name (optional): ", ref FeedLinkName);
	LabelAndTextField("Link Desc (optional): ", ref FeedLinkDescription);
	LabelAndTextField("Link Caption (optional): ", ref FeedLinkCaption);
	LabelAndTextField("Picture (optional): ", ref FeedPicture);
	LabelAndTextField("Media Source (optional): ", ref FeedMediaSource);
	LabelAndTextField("Action Name (optional): ", ref FeedActionName);
	LabelAndTextField("Action Link (optional): ", ref FeedActionLink);
	LabelAndTextField("Reference (optional): ", ref FeedReference);*/

	/*public string FeedToId = "";
	public string FeedLink = cubezFree;
	public string FeedLinkName = "Play CubeZ";
	public string FeedLinkCaption = "Play CubeZ";
	public string FeedLinkDescription = "";
	public string FeedPicture = "";
	public string FeedMediaSource = "";
	public string FeedActionName = "";
	public string FeedActionLink = "";
	public string FeedReference = "";
	public bool IncludeFeedProperties = true;
	private Dictionary<string, string[]> FeedProperties = new Dictionary<string, string[]>();*/
	
	public void CallFBFeed(int score, string levelName)
	{
		string postCaption;

		if (score == 0) {
			postCaption = "I just completed " + CurrentLevel.category.ToString() + " level: " + levelName+". You should try it too!";
		} else {
			postCaption = "I just completed " + CurrentLevel.category.ToString() + " level: " + levelName + " with a score of " + score.ToString()+". You should try it too!";
		}

		/*Dictionary<string, string[]> feedProperties = null;
		if (IncludeFeedProperties)
		{
			feedProperties = FeedProperties;
		}*/
		FB.Feed(
			//toId: FeedToId,
			link: cubezFree,
			linkName: "CubeZ Level Completed!",
			linkCaption: postCaption,
			//linkDescription: FeedLinkDescription,
			picture: "http://i58.tinypic.com/33m0mj9.png",
			//mediaSource: FeedMediaSource,
			//actionName: FeedActionName,
			//actionLink: FeedActionLink,
			//reference: FeedReference,
			//properties: feedProperties,
			callback: Callback
			);

	}


	void Callback(FBResult result)
	{

		var responseObject =  Facebook.MiniJSON.Json.Deserialize(result.Text) as Dictionary<string, object>;

		object cancelled;
		if (responseObject.TryGetValue ("cancelled", out cancelled))
		{
			if( (bool)cancelled == true )
			{
		
			
			}
			else
			{
			
			}
		}
		else
		{
		
		}

		if (String.IsNullOrEmpty(result.Error)) {
				Application.LoadLevel (3);
		}

	}
	#endregion

	#region twitter
	const string TwitterAddress = "http://twitter.com/intent/tweet";
	public void Tweet(int score, string levelName, string lang="en")
	{

		string postCaption;
		if (score == 0) {
			postCaption = "I just completed " + CurrentLevel.category.ToString() + " level: " + levelName+" on CubeZ. Try it at ";
		} else {
			postCaption = "I just completed " + CurrentLevel.category.ToString() + " level: " + levelName + " with a score of " + score.ToString()+" on CubeZ. Try it at ";
		}

		Application.OpenURL(TwitterAddress +
		                    "?text=" + WWW.EscapeURL(postCaption) +
		                    "&amp;url=" + WWW.EscapeURL(cubezFree) +
		                    "&amp;lang=" + WWW.EscapeURL(lang));


	}
	#endregion

	void Awake() {
		if (instance != null && instance != this) {
			DestroyImmediate(this.gameObject);
			return;
		} else {
			instance = this;
		}
		DontDestroyOnLoad(this.gameObject);
		songPlayer = GetComponent<SongPlayer> ();

		levelScripts = new List<LevelScript> (levels.Count);
		for (int i = 0; i < CATEGORY_COUNT; i++) 
		{
			levelsCategorized[i] = new List<LevelScript>();
		}
		
		foreach (GameObject level in levels) {
			LevelScript ls = level.GetComponent<LevelScript>();
			
			levelsCategorized[(int)ls.category].Add(ls);
			levelScripts.Add(ls);
		}

		GlobalData.Initialise ();

		unlockedLevels = new bool[levels.Count];
		//completedLevels = new int[levels.Count];
		ResetCompleted ();
		unlockedLevels [0] = true;

		LoadPrefs ();
		LoadLevelProgress ();
		CallFBInit ();
		UnlockAllLevels ();
	}

	void UnlockAllLevels()
	{
		for (int i = 0; i < unlockedLevels.Length; i++)
			unlockedLevels [i] = true;
	}

	void ResetCompleted()
	{
		completedLevels = new int[levels.Count];

		for (int i = 0; i < levels.Count; i++) {
			completedLevels[i] = -1;
		}
	}

	public void PopUpMenu()
	{
		GetComponent<PopupMenu>().enabled = !GetComponent<PopupMenu>().enabled;
		LockGameControls = GetComponent<PopupMenu>().enabled;
	}

	void Update()
	{
		if (quitting)
						return;

		if (Input.GetKeyUp (KeyCode.Menu) || (Application.platform == RuntimePlatform.WindowsEditor && Input.GetKeyUp(KeyCode.F1))) 
		{
			if (gameLevel)
			{
				PopUpMenu();
			}
			else
			{
				Application.LoadLevel(0);
			}

		}

		#if UNITY_ANDROID
		if (!GetComponent<StartAppBackPlugin> ().enabled  && Input.GetKeyUp (KeyCode.Escape)) {
			quitting = true;
			Application.Quit();
		}
		#else
		if (Input.GetKeyUp (KeyCode.Escape)) {
			quitting = true;
			Application.Quit();
		}
		#endif



		if (mustSave) {
			mustSave = false;
			PlayerPrefs.Save();
		}
	}



	public void SaveCurrentLevel(int score)
	{
		if (!gameLevel) {
			return;
		}
		ClearLevelSave ();
		PlayerPrefs.SetInt ("Current", CurrentIndexUncat);
		GameObject cubeGroup = GameObject.Find ("CubeGroup");
		PlayerPrefs.SetInt ("CubeCount", cubeGroup.transform.childCount);
		PlayerPrefs.SetInt ("Score", score);

		int c = 0;
		foreach (Transform cube in cubeGroup.transform) 
		{
			PlayerPrefs.SetInt("Cube"+c.ToString(),cube.gameObject.GetComponent<CubeBehaviour>().On?1:0);
			c++;
		}
		PlayerPrefs.Save ();

	}

	public bool HasLevelSave()
	{
		return PlayerPrefs.HasKey ("Current");
	}

	public void LoadSavedLevel()
	{
		if (HasLevelSave()) {
			//CurrentLevel = levelScripts[];
			LoadLevel(PlayerPrefs.GetInt ("Current"));
			//int scene = );
			//Application.LoadLevel(scene);
		}
	}

	public void ClearLevelSave()
	{
		if (HasLevelSave()) {
			PlayerPrefs.DeleteKey ("Current");
			int cubeCount = PlayerPrefs.GetInt ("CubeCount");
			PlayerPrefs.DeleteKey ("CubeCount");
			PlayerPrefs.DeleteKey("Score");
			for (int c = 0; c < cubeCount; c++) {
				PlayerPrefs.DeleteKey ("Cube" + c);
			}
		}
	}

	public void ClearLevelData()
	{
		PlayerPrefs.DeleteAll ();
		unlockedLevels = new bool[levels.Count];
		unlockedLevels [0] = true;
		ResetCompleted ();
		//completedLevels [0] = 0;
		SaveData ();
	}

	public bool IsLowestLevel(int level)
	{
		return (level == Level0);
	}


	public bool SocialShare(int score, string levelName)
	{
		bool waitFeed = false;


		Tweet (score, levelName, cubezFree);

		return waitFeed;
	}

	public bool LevelCompleted(int score)
	{
		//bool waitFeed = SocialShare(score, CurrentLevel.levelName);

		victoryScore = score;
		highScore = false;
		int currentIndex = FindIndexInUncat (CurrentLevel);
		completedLevels [FindIndexInUncat (CurrentLevel)] = score;
		bool hasKey = PlayerPrefs.HasKey ("Completed" + currentIndex);

		if (!hasKey || (hasKey && PlayerPrefs.GetInt ("Completed" + currentIndex) < score)) {
			PlayerPrefs.SetInt ("Completed" + currentIndex, score);
			highScore = true;
		}
		SaveData ();

		return false;
	}
	

	public void LoadPrefs()
	{
		songPlayer.SetAllowedSongs(Songs.both);
		themeDark = false;
		effectsOn = true;
		SoundEnabled = true;
		if (PlayerPrefs.HasKey ("Music")) {
			int musicHint = PlayerPrefs.GetInt("Music");
			songPlayer.SetAllowedSongs((Songs)(musicHint));
		}
		if (PlayerPrefs.HasKey ("Sound")) {
			SoundEnabled = (PlayerPrefs.GetInt("Sound")==1);
		}
		if (PlayerPrefs.HasKey ("Theme")) {
			themeDark = (PlayerPrefs.GetInt("Theme")==1);
		}

		if (PlayerPrefs.HasKey ("Effects")) {
			effectsOn = (PlayerPrefs.GetInt("Effects")==1);
		}
	}

	public void LoadLevelProgress()
	{
		for (int i = 0; i < levels.Count; i++) {
			if (PlayerPrefs.HasKey("Unlocked"+i))
			{
				unlockedLevels[i] = true;

			}

			if (PlayerPrefs.HasKey("Completed"+i))
			{
				completedLevels[i] = PlayerPrefs.GetInt("Completed"+i);
			}
		}
	}
	
	public void SaveData()
	{
		mustSave = true;
	}

	public void SavePrefs()
	{
		PlayerPrefs.SetInt ("Music", (int)songPlayer.allowedSongs);
		PlayerPrefs.SetInt ("Sound", SoundEnabled?1:0);
		PlayerPrefs.SetInt ("Theme", themeDark ? 1 : 0);
		PlayerPrefs.SetInt ("Effects", effectsOn ? 1 : 0);
		SaveData ();
	}

	public void StopMusic()
	{
		songPlayer.StopSongs ();
	}

	public void StartMusic()
	{
		if (MusicEnabled) {
			songPlayer.PlaySong();
		}
	}
	

	public void SetMusicState(Songs songs)
	{
		if (!enabled) {
			songPlayer.StopSongs();
		} else {
			songPlayer.SetAllowedSongs(songs);
			songPlayer.StopSongs();
			songPlayer.PlaySong();
		}
	}

	public void ReloadCurrentLevel()
	{
		Application.LoadLevel (CurrentLevel.OffsetScene);
	}

	public bool LoadLowestUnlockable()
	{
	
		for (int i = 0; i < unlockedLevels.Length-1; i++) {
			if (unlockedLevels[i] && !unlockedLevels[i+1])
			{
				CurrentLevel = levelScripts [i];
				Application.LoadLevel (CurrentLevel.OffsetScene);
				return true;
			}
		}
	
		return false;
	}

	public void LoadLevel(LevelScript level)
	{
		CurrentLevel = level;
		Application.LoadLevel (CurrentLevel.OffsetScene);
	}

	public void LoadLevel(int index)
	{
		CurrentLevel = levelScripts [index];
		Application.LoadLevel (CurrentLevel.OffsetScene);
	}

	public void LoadLevel(int index, Category category)
	{
		CurrentLevel = levelsCategorized [(int)category] [index];
		Application.LoadLevel (CurrentLevel.OffsetScene);
	}


	public bool LoadLowestUncompleted()
	{
		
		for (int i = 0; i < completedLevels.Length; i++) {
			if (completedLevels[i] == -1)
			{
				CurrentLevel = levelScripts [i];
				Application.LoadLevel (CurrentLevel.OffsetScene);
				return true;
			}
		}
	
		return false;
	}



	public void UnlockNextLevels(bool bonusUnlock)
	{
		int indexUncat = FindIndexInUncat (CurrentLevel);

		if (indexUncat+1 < unlockedLevels.Length) {
			if (!unlockedLevels[indexUncat+1])
			{
				unlockedLevels[indexUncat+1] = true;
				PlayerPrefs.SetInt("Unlocked"+(indexUncat+1),1);

				recentlyUnlockedLevels.Add(levelScripts[indexUncat+1]);
			}
		}

		if (bonusUnlock) {
				int indexCat = FindIndexInCat (CurrentLevel);
				LevelScript nextInCat = null;

				if (indexCat + 1 < levelsCategorized [(int)CurrentLevel.category].Count) {

					nextInCat = levelsCategorized [(int)CurrentLevel.category] [indexCat + 1];
				}


				if (nextInCat != null) {
					int indexUncatOfNextInCat = FindIndexInUncat (nextInCat);
					if (indexUncatOfNextInCat < unlockedLevels.Length) {
						if (!unlockedLevels [indexUncatOfNextInCat])
						{
							unlockedLevels [indexUncatOfNextInCat] = true;
							AddToRecentlyUnlocked(levelScripts[indexUncatOfNextInCat]);
							PlayerPrefs.SetInt ("Unlocked" + indexUncatOfNextInCat, 1);
						}
		
						
					}

				}
		}
		SaveData ();
	}

	void AddToRecentlyUnlocked(LevelScript level)
	{
		if (!recentlyUnlockedLevels.Contains (level)) {
			recentlyUnlockedLevels.Add (level);
		}
	}

	public int GetScore(LevelScript level)
	{
		return completedLevels [FindIndexInUncat (level)];
	}

	public bool IsUnlocked(LevelScript level)
	{

		return unlockedLevels [FindIndexInUncat (level)];
	}

	public int FindIndexInUncat(LevelScript level)
	{
		for (int i = 0; i < levelScripts.Count; i++) {
			if (levelScripts[i] == level)
			{
				return i;
			}
		}

		return -1;
	}

	public LevelScript FindNextInUncatScript(LevelScript level)
	{
		for (int i = 0; i < levelScripts.Count-1; i++) {
			if (levelScripts[i] == level)
			{
				return levelScripts[i+1];
			}
		}
		
		return null;
	}

	public LevelScript FindNextInCatScript(LevelScript level)
	{
		for (int i = 0; i < levelsCategorized[(int)_current_category].Count-1; i++) {
			if (levelsCategorized[(int)_current_category][i] == level)
			{
				return levelsCategorized[(int)_current_category][i+1];
			}
		}
		return null;
	}

	public LevelScript FindNextInUncatScript()
	{
		return FindNextInUncatScript (CurrentLevel);
	}
	
	public LevelScript FindNextInCatScript()
	{
		return FindNextInCatScript (CurrentLevel);
	}

	public int FindIndexInCat(LevelScript level)
	{
		for (int i = 0; i < levelsCategorized[(int)_current_category].Count; i++) {
			if (levelsCategorized[(int)_current_category][i] == level)
			{
				return i;
			}
		}
		
		return -1;
	}

	public void QuickLoad()
	{
		if (loadMode == LoadMode.LowestUncompleted) {
			if (!LoadLowestUncompleted ()) {
					Application.LoadLevel (0);
			}
		} else if (loadMode == LoadMode.Sequential) {
			int nextIndex = FindIndexInUncat (CurrentLevel) + 1;

			if (nextIndex < levels.Count) {
				LoadLevel (nextIndex);
			} else {
				Application.LoadLevel (0);
			}
		} else if (loadMode == LoadMode.Category) {
			int nextIndex = FindIndexInCat (CurrentLevel) + 1;
			
			if (nextIndex < levelsCategorized[(int)_current_category].Count) {
				LoadLevel (nextIndex, _current_category);
			} else {
				Application.LoadLevel (0);
			}
		}
	}

	public void ShareFacebook(string link, string pictureLink, string name,
	                          string caption, string description, string redirectUri)
	{
				Application.OpenURL (ShareUrl +
						"?app_id=" + AppId +
						"&amp;link=" + WWW.EscapeURL (link) +
						"&amp;picture=" + WWW.EscapeURL (pictureLink) +
						"&amp;name=" + WWW.EscapeURL (name) +
						"&amp;caption=" + WWW.EscapeURL (caption) +
						"&amp;description=" + WWW.EscapeURL (description) +
						"&amp;redirect_uri=" + WWW.EscapeURL (redirectUri));
	}

	void OnLevelWasLoaded(int level)
	{
		if (this != instance)
						return;
		currentManager = null;
		lastLoadedLevel = level;
		if (level >= Level0) {
			gameLevel = true;
			StartMusic();
		} else {
			gameLevel = false;
		}

		if (gameLevel) {
			currentManager = GameObject.Find ("CubeGroup").GetComponent<Manager> ();

			if (HasLevelSave () && PlayerPrefs.GetInt ("Current") == CurrentIndexUncat) {
				currentManager.LoadLevel (PlayerPrefs.GetInt("Score"));

			} else {
				currentManager.Generate (CurrentLevel);
			}
		}
	}

}
