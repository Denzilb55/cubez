﻿using UnityEngine;
using System.Collections;

public class LevelCardScript : MonoBehaviour {
	public GameObject titleObject;
	public GameObject levelNumObject;
	public GameObject scoreObject;
	private LevelScript linkedLevel;
	private bool IsUnlocked;
	private static Color lockedCol = new Color(40/255f,40/255f,40/255f);
	public Material lockedBlack;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}



	public void LinkCard(LevelScript level, int levelNum, int score, bool isUnlocked)
	{
		titleObject.GetComponent<TextMesh> ().text = level.levelName;
		TextMesh levelNumMesh = levelNumObject.GetComponent<TextMesh> ();
		levelNumMesh.text = (1+levelNum).ToString ();
		if (score != -1) {
			scoreObject.GetComponent<TextMesh> ().text = "Score: " + score.ToString ();
		} else {
			scoreObject.GetComponent<TextMesh> ().text = "No Score";
		}
		linkedLevel = level;
		IsUnlocked = isUnlocked;

		if (!isUnlocked) {
			gameObject.renderer.material = lockedBlack;
		//	gameObject.renderer.material.color = lockedCol;
			titleObject.renderer.enabled = false;
			scoreObject.renderer.enabled = false;
			levelNumMesh.fontSize += 40;
			levelNumObject.transform.Translate(Vector3.up * 8);
		}
	}

	public void Activate()
	{
		if (IsUnlocked) {
			PersistentScript.Instance.loadMode = LoadMode.Category;
			PersistentScript.Instance.ClearLevelSave ();
			PersistentScript.Instance.LoadLevel (linkedLevel);
		}
	}


}
