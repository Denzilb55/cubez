﻿using UnityEngine;
using System.Collections;

public class MenuSounds : MenuBehaviour {
	

	private PersistentScript ps;
	public override void Execute ()
	{
		base.Execute ();
		
		ps.SoundEnabled = !ps.SoundEnabled;
		SetText ();
		ps.SavePrefs ();
	}
	
	string BoolText(bool enable)
	{
		if (enable) {
			return "On";
		} else {
			return "Off";
		}
	}
	
	private void SetText()
	{
		textMesh.text = "Sound: " + BoolText (ps.SoundEnabled);
	}
	
	void Start () {
		ps = (GameObject.FindGameObjectWithTag ("Persistent")).GetComponent<PersistentScript>();
		textMesh = GetComponent<TextMesh> ();
		SetText ();
	}
}
