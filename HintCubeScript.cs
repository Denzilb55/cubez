﻿using UnityEngine;
using System.Collections;

public class HintCubeScript : MonoBehaviour {


	private bool showHint = false;
	public Font font;
	private bool clicked = false;
	public Light haloLight;
	private float sinePeriod = 2.5f;
	private float t;
	private float haloRange = 4.5f;
	float minRange = 3;


	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!clicked) {
			haloLight.range = (haloRange - minRange)/2f*(Mathf.Cos(t / sinePeriod * 2 * Mathf.PI) + 1) + minRange;
			haloLight.intensity = haloLight.range/5f * (12)-8;
			t += Time.deltaTime;
		}
	}

	public void ToggleHint()
	{
		showHint = !showHint;
		GetComponent<HintGUI>().enabled = showHint;
		clicked = true;
		haloLight.enabled = false;
	}

}
