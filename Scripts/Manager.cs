﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Manager : MonoBehaviour {
	public List<GameObject> cubes = new List<GameObject>();
	private PersistentScript ps;
	//public bool StartRandom;
//	public int randomIterations; 
	private bool victory = false;
	public GameObject fireworksParticles;
	private bool fireWorksReady = true;
	private float deltaFW = 0;
	private float periodFW = 0.25f;
	private float periodFWMax = 0.6f;
	//public int randomSeed = -1;

	private float victorySoundPeriod = 3f;
	private float victoryPeriod = 0f;

	public int clickCount = 0;
	public TextMesh titleMesh;
	public TextMesh scoreMesh;
	public string levelName;
	private int Score;
	public int Multiplier = 1;
	public float ScrollSpeed = 0.04f;
	private bool hasGenerated = false;

	private LevelScript level;
	private GameObject lastTapped;
	public GameObject homeCube;

	public bool isVictorious = false;

	// Use this for initialization
	void Start () {
		ps = (GameObject.FindGameObjectWithTag ("Persistent")).GetComponent<PersistentScript>();
		titleMesh.text = ps.CurrentLevel.category.ToString() + " Level " + (ps.CurrentIndexCat + 1) + ": " + ps.CurrentLevel.levelName;

		//RegisterCubes ();
		/*if (!hasGenerated) {
			Generate();
		}*/

		if (ps.themeDark) {
			Camera.main.backgroundColor = new Color (13 / 255f, 13 / 255f, 38 / 255f);
		} else {
			Camera.main.backgroundColor = new Color (0 / 255f, 157 / 255f, 5 / 255f);
		}

		homeCube = GameObject.FindGameObjectWithTag ("Home");

		ResolveGraphics ();
	}

	private void ResolveGraphics()
	{

		Vector3 v3Pos =  new Vector3(0.53f, 0.45f, 7f);
		transform.position = Camera.main.ViewportToWorldPoint (v3Pos);

		titleMesh.fontSize = 45;
		scoreMesh.fontSize = 40;
		titleMesh.fontStyle = FontStyle.Normal;
		scoreMesh.fontStyle = FontStyle.Normal;

		if (ps.themeDark) {
			titleMesh.color = new Color(0.82f,0.82f,0.82f);
			scoreMesh.color = titleMesh.color;
		} else {
			titleMesh.color = Color.blue;
			scoreMesh.color = new Color(0.93f,0.1f,0.1f);
		}

		titleMesh.gameObject.transform.position = Camera.main.ViewportToWorldPoint (new Vector3 (0.2f, 0.96f, 50));
		scoreMesh.gameObject.transform.position = Camera.main.ViewportToWorldPoint (new Vector3 (0.2f, 0.86f, 50));

		if (ps.CurrentLevel.hint != null && ps.CurrentLevel.hint != "") {
			GameObject hintCube = Instantiate(ps.hintCubeTemplate) as GameObject;
			hintCube.GetComponent<HintGUI>().Hint = ps.CurrentLevel.hint;
			hintCube.transform.position = Camera.main.ViewportToWorldPoint(new Vector3 (0.21f, 0.5f, 10));
		}


		StartGlowCubes ();
	}

	void OnApplicationPause(bool pauseStatus) {
		if (pauseStatus) {
			ps.SaveCurrentLevel(Score);
		}
	}

	public void LoadLevel(int score)
	{
		ReregisterCubes ();
		Score = score;
		UpdateScore ();
		hasGenerated = true;
		int cubeCount = PlayerPrefs.GetInt ("CubeCount");
		int c = 0;
		ps = (GameObject.FindGameObjectWithTag ("Persistent")).GetComponent<PersistentScript>();
		foreach (Transform cube in gameObject.transform) {
			bool cubeState = PlayerPrefs.GetInt ("Cube" + c.ToString ()) == 1;
			cube.gameObject.GetComponent<CubeBehaviour> ().SetState(cubeState) ;
			c++;
		}
		if (CheckVictory ()) {
			isVictorious = true;
			Invoke("DoVictory",0.8f);
		}

	//	ResolveGraphics ();
	}

	public void StartGlowCubes()
	{
		foreach (var cube in cubes) 
		{
			CCube(cube).StartAction();
		}
	}

	private void RegisterCubes()
	{
		cubes.Clear ();
		foreach (Transform child in transform) 
		{
			
			GameObject childObject = child.gameObject;
			cubes.Add(childObject);
		}
	}

	private void ReregisterCubes()
	{
		cubes.Clear ();
		foreach (Transform child in transform) 
		{
			
			GameObject childObject = child.gameObject;
			cubes.Add(childObject);
			childObject.GetComponent<CubeBehaviour>().SetState(false);
		}
	}

	public void Generate()
	{
		if (level != null) {
			Generate(level);
		} else {
			UnityEngine.Debug.Log("GENERATE ERROR");
		}

	}

	public void Generate(LevelScript level)
	{
		this.level = level;
		RegisterCubes ();
		hasGenerated = true;

		if (level.randomSeed != -1)
		{
			Random.seed = level.randomSeed;
		}
		int countOut = 0;
		while (Randomize (level.iterations))
		{
			Random.seed = Random.seed+1;
			if (countOut++ > 50)
			{
				break;
			}
		}

		Random.seed = (int)Time.time;
		bool invert = Random.Range (0, 2) == 1;

		if (invert) {
				foreach (GameObject cube in cubes) {
					CubeBehaviour cs = cube.GetComponent<CubeBehaviour>();
					cs.On = !cs.On;
				}
		}

		if (level.score == 0) {
			Score = level.iterations * 2;
		} else {
			Score = level.score;
		}
		UpdateScore ();

	}

	void UpdateScore()
	{
		if (Multiplier != 1) {
			scoreMesh.text = "Score: " + Score + " x " + Multiplier;
		} else {
			scoreMesh.text = "Score: " + Score;
		}
	}
	

	// Update is called once per frame
	void Update () {
		if (ps.LockGameControls || isVictorious) {
			return;
		}


		if (Input.GetKeyUp (KeyCode.A)){
			DoVictory();
		}

		bool clicked = false;
		if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor) {
			if (Input.GetMouseButtonDown(0))
			{
				clicked = true;
			}

		}

		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
			if (Input.touchCount == 1 && Input.GetMouseButtonDown(0))
			{
				clicked = true;
			}
			
		}

		if(clicked)
		{
			if (victory)
			{

				ps.QuickLoad();
			


			}
			else
			{
				Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
				RaycastHit hit;
				
				if( Physics.Raycast( ray, out hit, 100 ) )
				{
					ObjectClicked(hit);
				}
			}
		}

	}

	public void ReturnToMenu()
	{
		ps.SaveCurrentLevel(Score);
		Application.LoadLevel(0);
		ps.LockGameControls = false;
	}

	private void ObjectClicked(RaycastHit hit)
	{
		if (hit.transform.tag == "Hint") {
			hit.transform.gameObject.GetComponent<HintCubeScript> ().ToggleHint ();
		} else if (hit.transform.tag == "Home") {
			ps.PopUpMenu();
		}
		else
		{
			GameObject thisTapped = hit.transform.gameObject;



			var cube = thisTapped.GetComponent<CubeBehaviour>();
			if (cube != null)
			{
				clickCount++;
		

				UpdateScore();
				if (cube.SwapLinked(ps.effectsOn))
				{
					if (lastTapped == thisTapped)
					{
						Score += 1;
						lastTapped = null;
					}
					else
					{
						if (Score > 0)
						{
							Score -= 1;
							
							lastTapped = thisTapped;
						}
					}
				}
				cube.Sparkle();
				if (ps.SoundEnabled)
				{
					audio.Play();
				}
				if (CheckVictory()) 
				{
					isVictorious = true;
					Invoke("DoVictory",0.8f);
				}

			}
		
		}
	}

	public void DoVictory()
	{
		victory = true;
		bool waitFeed = ps.LevelCompleted (Score);
		ps.UnlockNextLevels (Score > 0);
		ps.ClearLevelSave ();
		if (!waitFeed) {
			Application.LoadLevel (3);
		}

	}
	

	public bool CheckVictory()
	{
		bool b_or = false;
		bool b_and = true;

		foreach (var c in cubes) 
		{
			b_and &= c.gameObject.GetComponent<CubeBehaviour>().On;
			b_or |= c.gameObject.GetComponent<CubeBehaviour>().On;
		}


		return (!b_or || b_and); 
	}

	public GameObject GetRandomCube()
	{
		int num = UnityEngine.Random.Range(0,cubes.Count);
		return cubes[num];
	}

	public CubeBehaviour CCube(GameObject cube)
	{
		return cube.GetComponent<CubeBehaviour> ();
	}

	public bool Randomize(int iterations)
	{

		for (int i = 0; i < iterations; i++) {
			CubeBehaviour cube = CCube(GetRandomCube());
			int num = UnityEngine.Random.Range(0,cubes.Count);

			cube.SwapLinked(false);
		}

		return CheckVictory ();
	}
}
