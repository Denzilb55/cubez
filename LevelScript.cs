﻿using UnityEngine;
using System.Collections;

public class LevelScript : MonoBehaviour {
	public Category category;
	public int Scene;

	public int OffsetScene
	{
		get {return Scene + PersistentScript.Instance.Level0 - 4;}
	}

	public string levelName;
	public int randomSeed;
	public int iterations;
	public int score = 0;
	public string hint;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


}
