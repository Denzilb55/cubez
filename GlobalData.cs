﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ColorIndex {Red = 0, BlueLight, White, Black}

public class GlobalData  {
	
	public static Material materialRed;
	public static Material materialBlueLight;
	public static Material materialWhite;
	public static Material materialBlack;

	public static List<Material> materials = new List<Material> ();
	public static List<Color> colors = new List<Color> ();

	public static Color colorRed;
	public static Color colorBlueLight;
	public static Color colorWhite;
	public static Color colorBlack;
	
	public static void Initialise()
	{
		materialRed = new Material (Shader.Find("Mobile/Diffuse"));
		materialBlueLight = new Material (Shader.Find("Mobile/Diffuse"));
		materialWhite = new Material (Shader.Find("Mobile/Diffuse"));
		materialBlack = new Material (Shader.Find("Mobile/Diffuse"));
		
		materialRed.SetTexture ("_MainTex", Resources.Load<Texture> ("Textures/Colors/red"));
		materialBlueLight.SetTexture ("_MainTex", Resources.Load<Texture> ("Textures/Colors/blue"));
		materialWhite.SetTexture ("_MainTex", Resources.Load<Texture> ("Textures/Colors/white"));
		materialBlack.SetTexture ("_MainTex", Resources.Load<Texture> ("Textures/Colors/black"));

		materials.Add (materialRed);
		materials.Add (materialBlueLight);
		materials.Add (materialWhite);
		materials.Add (materialBlack);

		colorRed = new Color (254/255f,18/255f,2/255f);
		colorBlueLight = new Color (64/255f,195/255f,252/255f);
		colorWhite = new Color (230/255f,230/255f,230/255f);
		colorBlack = new Color (80/255f,80/255f,80/255f);

		colors.Add (colorRed);
		colors.Add (colorBlueLight);
		colors.Add (colorWhite);
		colors.Add (colorBlack);
	}

}
