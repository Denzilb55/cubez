﻿using UnityEngine;
using System.Collections;

public class PopupMenu : MonoBehaviour {

	private PersistentScript ps;

	float originalWidth = 793f;  // define here the original resolution
	float originalHeight = 395f; // you used to create the GUI contents 
	private Vector3 scale;
	public Font buttonFont;
	public int buttonFontSize;
	public Color buttonTextColor;
	public Color buttonTextColor2;
	private Manager manager;

	// Use this for initialization
	void Start () {
		ps = GetComponent<PersistentScript> ();
		manager = GameObject.Find ("CubeGroup").GetComponent<Manager> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{
		GUIStyle buttonStyle = new GUIStyle (GUI.skin.button);
		buttonStyle.fontSize = buttonFontSize;
		buttonStyle.font = buttonFont;
		buttonStyle.normal.textColor = buttonTextColor;
		buttonStyle.hover.textColor = buttonTextColor;
		buttonStyle.focused.textColor = buttonTextColor;
		buttonStyle.onNormal.textColor = buttonTextColor;
		buttonStyle.onHover.textColor = buttonTextColor;
		buttonStyle.onFocused.textColor = buttonTextColor;
		buttonStyle.active.textColor = buttonTextColor2;
		buttonStyle.onActive.textColor = buttonTextColor2;
		//buttonStyle.
		//UnityEngine.Debug.Log (Screen.width + " " + Screen.height);

		scale.y = Screen.height/originalHeight; // calculate vert scale
		scale.x = scale.y; // this will keep your ratio base on Vertical scale
		scale.z = 1;
		float scaleX = Screen.width/originalWidth; 

		var svMat = GUI.matrix; // save current matrix
		// substitute matrix - only scale is altered from standard
		GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, scale);
		//GUI.matrix = Matrix4x4.TRS(new Vector3( (scaleX - scale.y) / 2 * originalWidth, 0, 0), Quaternion.identity, scale);
		// draw your GUI controls here:
		//GUI.Box(Rect(10,10,200,50), "Box");
		//GUI.Button(Rect(400,180,230,50), "Button");


		int yAnchor = (int)originalHeight - 104;//Screen.height - 0;
		Rect retryPos = new Rect(2,yAnchor,130,50);
		if (GUI.Button (retryPos,"Retry",buttonStyle))
		{
			ps.ClearLevelSave();
			//GameObject.Find ("CubeGroup").GetComponent<Manager> ().Generate ();
			ps.ReloadCurrentLevel();
			this.enabled = false;
			ps.LockGameControls = false;
		}

		Rect MenuPos = new Rect(2,yAnchor + 52,130,50);
		if (GUI.Button (MenuPos,"Menu",buttonStyle))
		{
			this.enabled = false;
			manager.ReturnToMenu();
		}


		Event e = Event.current;

		if (e.type == EventType.MouseDown)
		{
			ps.LockGameControls = false;
			this.enabled = false;
		}

		//...
		// restore matrix before returning
		GUI.matrix = svMat; // restore matrix
	}
}
