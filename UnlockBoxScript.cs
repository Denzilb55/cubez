﻿using UnityEngine;
using System.Collections;

public class UnlockBoxScript : MonoBehaviour {

	public GameObject category;
	public GameObject levelName;
	private LevelScript level;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void FillBox(LevelScript level)
	{
		this.level = level;
		category.GetComponent<TextMesh> ().text = level.category.ToString();
		levelName.GetComponent<TextMesh> ().text = level.levelName;
	}

	public void Activate()
	{
		PersistentScript.Instance.LoadLevel (level);
	}
}
