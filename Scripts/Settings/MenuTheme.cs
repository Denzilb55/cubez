﻿using UnityEngine;
using System.Collections;

public class MenuTheme : MenuBehaviour {

	private PersistentScript ps;
	public override void Execute ()
	{
		base.Execute ();
		
		ps.themeDark = !ps.themeDark;
		SetText ();
		ps.SavePrefs ();
	}
	
	string BoolText(bool enable)
	{
		if (enable) {
			return "Dark";
		} else {
			return "Light";
		}
	}
	
	private void SetText()
	{
		textMesh.text = "Theme: " + BoolText (ps.themeDark);
	}
	
	void Start () {
		ps = (GameObject.FindGameObjectWithTag ("Persistent")).GetComponent<PersistentScript>();
		textMesh = GetComponent<TextMesh> ();
		SetText ();
	}
}
