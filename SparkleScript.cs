﻿using UnityEngine;
using System.Collections;

public class SparkleScript : MonoBehaviour {


	void Start () {

	}

	private void _DestroySelf()
	{
		Destroy (gameObject);
	}

	public void DestroySelf(float seconds)
	{
		Invoke ("_DestroySelf", seconds);
	}

	public void Initialize(Color color, float life)
	{
		DestroySelf (life);
		particleSystem.startColor = color;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
