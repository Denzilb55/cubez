﻿using UnityEngine;
using System.Collections;

public class HintGUI : MonoBehaviour {

	//private HintScript hint;
	private HintCubeScript hintCube;
	float originalWidth = 793f;  // define here the original resolution
	float originalHeight = 395f; // you used to create the GUI contents 
	private Vector3 scale;
	private Texture2D background;
	public string Hint;

	void Start()
	{
		hintCube = GetComponent<HintCubeScript> ();
	//	hint = hintCube.HintObject.GetComponent<HintScript>();
		background = MakeTex( 2, 2, new Color( 1f, 1f, 0f, 0.93f ) );
	}


	
	private Texture2D MakeTex( int width, int height, Color col )
	{
		Color[] pix = new Color[width * height];
		for( int i = 0; i < pix.Length; ++i )
		{
			pix[ i ] = col;
		}
		Texture2D result = new Texture2D( width, height );
		result.SetPixels( pix );
		result.Apply();
		return result;
	}

	void OnGUI()
	{
		int width = 140;
		int height = 200;

		scale.y = Screen.height/originalHeight; // calculate vert scale
		scale.x = scale.y; // this will keep your ratio base on Vertical scale
		scale.z = 1;
		float scaleX = Screen.width/originalWidth; 
		
		var svMat = GUI.matrix; // save current matrix
		// substitute matrix - only scale is altered from standard
		GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, scale);

		Rect hintPosition = new Rect (4, 4, width, height );

		GUIStyle style = new GUIStyle (GUI.skin.box);
		style.wordWrap = true;
		style.font = hintCube.font;
		style.alignment = TextAnchor.MiddleCenter;
		style.fontSize = 26;
		style.normal.background = background;
		style.normal.textColor = Color.black;

		GUI.Box (hintPosition, Hint, style);
	}
}
