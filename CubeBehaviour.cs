﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CubeBehaviour : MonoBehaviour {
	public List<GameObject> linked = new List<GameObject>();
	
	private float GlowPeriod = 0.7f;
	private float currentGlow = 0f;
	private GameObject glowObject;
	public GameObject HaloObject;
	private Light haloLight;
	public GameObject SparkleObject;
	private float initialGlowRadius = 0.8f;
	private float finalGlowRadius = 1.8f;
	
	private ColorIndex colorIndex;
	
	public CubeScript CCube 
	{
		get{
			return this.GetComponent<CubeScript>();
		}
	}



	public bool On;
	
	// Use this for initialization
	


	// Update is called once per frame
	void Update () {
		if (glowObject != null) {
			currentGlow += Time.deltaTime;
			
			float timeFraction = currentGlow/GlowPeriod;
			
			float glowUp = (timeFraction*3f);
			if (glowUp >= 1)
			{
				glowUp = 1;
			}
			
			if (timeFraction > 0.5)
			{
				float alpha = 1 - timeFraction*2 + 0.5f;
				alpha = alpha > 1?1:alpha;
				haloLight.color = new Color(haloLight.color.r,haloLight.color.g,haloLight.color.b,alpha);
			}
			
			haloLight.range = initialGlowRadius + (finalGlowRadius-initialGlowRadius)*glowUp;
			
			
			if (currentGlow > GlowPeriod)
			{
				StopGlow();
			}
		}
	}
	
	public void SetState(bool state)
	{
		On = state;
		CheckColor ();
	}
	
	public void Swap(bool doGlow)
	{
		On = !On;
		CheckColor ();
		
		if (doGlow) {
			Glow ();
		}

	}
	
	public void StopGlow()
	{
		if (glowObject != null) {
			Destroy(glowObject);
			glowObject = null;
			haloLight = null;
		}
		
	}
	
	public void Glow()
	{
		
		if (glowObject == null) {
			glowObject = GameObject.Instantiate (HaloObject, transform.position, transform.rotation) as GameObject;
			glowObject.transform.Translate (new Vector3 (0, 0.5f, 0), transform);
			haloLight = glowObject.GetComponent<Light> ();
			haloLight.range = initialGlowRadius;
		}
		haloLight.color = GlobalData.colors[(int)colorIndex];
		currentGlow = 0f;
	}
	
	public void Sparkle()
	{
		if (!PersistentScript.Instance.effectsOn) {
			return;
		}
		GameObject go = GameObject.Instantiate (SparkleObject, transform.position, transform.rotation) as GameObject;
		go.GetComponent<SparkleScript> ().DestroySelf (0.6f);
	}
	
	public void Glow(float period)
	{
		GlowPeriod = period;
		Glow ();
	}
	
	public void StartAction()
	{
		CheckColor ();
		Glow (1f);
	}
	
	private void CheckColor()
	{
		if (PersistentScript.Instance.themeDark) {
			if (On) {
				AssignColor(ColorIndex.White);
			} else {
				AssignColor(ColorIndex.Black);
			}
		} else {
			if (On) {
				AssignColor(ColorIndex.Red);
			} else {
				AssignColor(ColorIndex.BlueLight);
			}
		}
		
	}

	private void AssignColor(ColorIndex colorIndex)
	{
		this.colorIndex = colorIndex;
		renderer.sharedMaterial = GlobalData.materials [(int)colorIndex];
	}
	
	public bool SwapLinked(bool doGlow)
	{
		if (linked.Count == 0) {
			return false;
		}
		foreach (var go in linked) {
			go.GetComponent<CubeBehaviour>().Swap(doGlow);
		}

		return true;
	}
}
