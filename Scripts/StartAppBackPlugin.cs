
using UnityEngine;
using System.Collections;
#if UNITY_ANDROID
using StartApp;
#endif

public class StartAppBackPlugin : MonoBehaviour{
	#if UNITY_ANDROID
	void Start () {
		StartAppWrapper.loadAd();	
    }
	void Update () {
		if (Input.GetKeyUp(KeyCode.Escape) ) {
			 showAdAndExit();
		}
    }
	
    void showAdAndExit() {	
		StartAppWrapper.showAdAndExit();
	}
	#endif
}
