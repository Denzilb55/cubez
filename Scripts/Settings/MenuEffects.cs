﻿using UnityEngine;
using System.Collections;

public class MenuEffects : MenuBehaviour {

	private PersistentScript ps;
	public override void Execute ()
	{
		base.Execute ();
		ps.effectsOn = !ps.effectsOn;
		SetText ();
		ps.SavePrefs ();
	}
	
	string BoolText(bool enable)
	{
		if (enable) {
			return "On";
		} else {
			return "Off";
		}
	}
	
	private void SetText()
	{
		textMesh.text = "Effects: " + BoolText (ps.effectsOn);
	}
	
	void Start () {
		ps = (GameObject.FindGameObjectWithTag ("Persistent")).GetComponent<PersistentScript>();
		textMesh = GetComponent<TextMesh> ();
		SetText ();
	}
}
