﻿using UnityEngine;
using System.Collections;

public class SocialFacebook : SocialBehaviour {


	public override void Execute()
	{
		if (FB.IsLoggedIn) {
			PersistentScript.Instance.CallFBFeed (score, levelName);
		} else {
			PersistentScript.Instance.CallFBLogin ();
		}
	}
}
