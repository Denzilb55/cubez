﻿using UnityEngine;
using System.Collections;

public class StartGame : MenuBehaviour {

	PersistentScript ps;


	void Start () {

		ps = (GameObject.FindGameObjectWithTag ("Persistent")).GetComponent<PersistentScript>();

		textMesh = GetComponent<TextMesh> ();


		if (!ps.HasLevelSave())
		{
			textMesh.text = "Start Game";
		}
		else
		{
			textMesh.text = "Continue";
		}
	}

	#region implemented abstract members of MenuBehaviour
	public override void Execute ()
	{
		base.Execute ();

		if (ps.HasLevelSave()) {
			ps.LoadSavedLevel();
		} 
		else{
			if (!ps.LoadLowestUncompleted())
			{
				ps.loadMode = LoadMode.Sequential;
				ps.LoadLevel(0);
			}
		}
	}


	#endregion

	// Use this for initialization

	

}
