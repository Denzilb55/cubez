﻿using UnityEngine;
using System.Collections;

public class MenuMusic : MenuBehaviour {

	private PersistentScript ps;
	private int state = 2;
	public override void Execute ()
	{
		base.Execute ();

		state++;
		if (state > (int)Songs.both) 
		{
			state = 0;
		}

		ps.SetMusicState ((Songs)(state));

		SetText ();
		ps.StartMusic ();
		ps.SavePrefs ();
	}

	string BoolText(bool enable)
	{
		if (enable) {
			return "On";
		} else {
			return "Off";
		}
	}

	private void SetText()
	{
		textMesh.text = "Music: " + MusicText();
	}

	private string MusicText()
	{
		switch (state) {
		case 0: return "Off";

		case 1: return "The Forest and The Trees";

		case 2: return "Fluffing a Duck";

		case 3: return "Both";
		}

		return "Error";
	}

	void Start () {
		ps = (GameObject.FindGameObjectWithTag ("Persistent")).GetComponent<PersistentScript>();
		textMesh = GetComponent<TextMesh> ();
		state = (int)ps.songPlayer.allowedSongs;
		SetText ();
	}
}
