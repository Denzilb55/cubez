﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {

	public MenuBehaviour tickledOption = null;
	private int resetTouch = 0;
	private float resetPeriod;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (resetTouch != 0) {
			resetPeriod += Time.deltaTime;
			if (resetPeriod > 1)
			{
				resetPeriod = 0;
				resetTouch = 0;
			}
		}

		Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
		RaycastHit hit;
		MenuBehaviour option = null;




		if (Input.GetMouseButtonUp (0)) {
			if( Physics.Raycast( ray, out hit, 500 ) )
			{

			
				if (hit.transform.tag == "Home") {
					Application.LoadLevel(0);
				}
				else 
				{
					option = hit.transform.gameObject.GetComponent<MenuBehaviour>();
					if (option != null && option == tickledOption)
					{
						if (option == tickledOption)
						{
							tickledOption = null;
							option.Execute();
						}
						else
						{
							tickledOption = null;
							tickledOption.Exit();
						}
					}
					else
					{
						if (option == null)
						{
							resetTouch++;
							resetPeriod = 0;

							
							if (resetTouch == 20)
							{
								resetTouch = 0;
								PersistentScript.Instance.ClearLevelData();
								
							}
						}
					}
				}
			}
			else
			{

				tickledOption.Exit();
				tickledOption = null;
			}
		}

		if (tickledOption != null) {
			//UnityEngine.Debug.Log("HAS TICKLED");
			if( Physics.Raycast( ray, out hit, 500 ) )
			{
				if (hit.transform.gameObject != tickledOption.gameObject)
				{
					tickledOption.Exit();
					tickledOption = null;

				}
			}
			else
			{
				tickledOption.Exit();
				tickledOption = null;
			}
		}

		if (Input.GetMouseButtonDown (0)) {

			if( Physics.Raycast( ray, out hit, 500 ) )
			{
				option = hit.transform.gameObject.GetComponent<MenuBehaviour>();
				if (option != null)
				{
					option.Tickle();
					tickledOption = option;
				}
			}
		}



		/*	if (option.tag == "Home") {
				Application.LoadLevel(0);
			}

			if (Input.GetMouseButtonDown (0)) 
			{
				option.Tickle();
				tickledOption = option;
			}
			else if (option == tickledOption && Input.GetMouseButtonUp (0))
			{
				tickledOption = null;
				option.Execute();
			}
			else if (option != tickledOption && Input.GetMouseButtonUp (0))
			{
				tickledOption = null;
				option.Exit();
			}
		} else if (tickledOption != null){
			tickledOption.Exit();
			tickledOption = null;
		}*/


	}
}
