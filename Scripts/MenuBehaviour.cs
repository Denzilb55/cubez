﻿using UnityEngine;
using System.Collections;

public class MenuBehaviour: MonoBehaviour  {
	protected TextMesh textMesh;
	protected static Color custom_blue;
	protected static Color custom_red;
	public int fontSize = 80;

	void Start () {
		textMesh = GetComponent<TextMesh> ();
		custom_blue = new Color (37f/255f, 24f/255f, 192f/255f);
		custom_red = new Color (210f/255f, 24f/255f, 19f/255f);
	}
	

	public virtual void Tickle ()
	{
		if (textMesh != null) {
			textMesh.color = custom_red;
			textMesh.fontSize = fontSize + 10;
		}
	}
	public virtual void Execute ()
	{
		Exit ();
	}
	public virtual void Exit ()
	{
		if (textMesh != null) {
			textMesh.color = custom_blue;
			textMesh.fontSize = fontSize;
		}
	}
}
