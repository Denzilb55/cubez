﻿using UnityEngine;
using System.Collections;

public class ScaleCamera : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
		/*foreach (Transform child in transform) {
			if (child.gameObject.tag == "GUI")
			{
				float x = child.position.x;
				float y = child.position.y;
				float z = child.position.z;

				float distFromCam = 30f;

				x = Camera.main.transform.position.x - (distFromCam*320/(float)(Screen.height));
				y = Camera.main.transform.position.y - distFromCam;
				z = Camera.main.transform.position.z +(distFromCam*240)/(float)(Screen.height);
				if (child.gameObject.name == "Score") z -= 4;
				child.position = new Vector3(x,y,z);
			}
		}*/

		float targetaspect = 3.0f / 2.0f;
		targetaspect = 1f;
		
		// determine the game window's current aspect ratio
		float windowaspect = (float)Screen.width / (float)Screen.height;
		
		// current viewport height should be scaled by this amount
		float scaleheight = windowaspect / targetaspect;
		
		// obtain camera component so we can modify its viewport
		Camera camera = Camera.main;
		
		// if scaled height is less than current height, add letterbox
		if (scaleheight < 1.0f)
		{  
			Rect rect = camera.rect;
			
			rect.width = 1.0f;
			rect.height = scaleheight;
			rect.x = 0;
			rect.y = (1.0f - scaleheight) / 2.0f;
			
			camera.rect = rect;
		}
		/*else // add pillarbox
		{
			float scalewidth = 1.0f / scaleheight;
			
			Rect rect = camera.rect;
			
			rect.width = scalewidth;
			rect.height = 1.0f;
			rect.x = (1.0f - scalewidth) / 2.0f;
			rect.y = 0;
			
			camera.rect = rect;
		}*/
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
