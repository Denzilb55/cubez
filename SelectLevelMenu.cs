﻿using UnityEngine;
using System.Collections;

public class SelectLevelMenu : MonoBehaviour {

	private MenuBehaviour tickledOption = null;
	private bool locked = false;

	// Use this for initialization
	void Start () {
		
	}

	public void NotifyUnlock()
	{
		locked = false;
	}

	public void NotifyLock()
	{
		locked = true;
	}

	// Update is called once per frame
	void Update () 
	{

		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;


		bool clicked = false;
		if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor) {
			if (Input.GetMouseButtonUp(0))
			{
				clicked = true;
			}
			
		}
		
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
			if (Input.touchCount == 1 && Input.GetMouseButtonUp(0))
			{
				clicked = true;
			}
			
		}

		if (clicked) {
		
			if (Physics.Raycast (ray, out hit, 500)) {
					GameObject gameObject = hit.transform.gameObject;
					if (!locked && gameObject.tag == "LevelCard") {
							gameObject.GetComponent<LevelCardScript> ().Activate ();
					} else if (gameObject.tag == "Home") {
							Application.LoadLevel (0);
					} 
			}

			locked = false;
		}
		

	}
}
