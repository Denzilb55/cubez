﻿using UnityEngine;
using System.Collections;
#if UNITY_ANDROID
using StartApp;
#endif
public class InterstitialAds : MonoBehaviour {
	#if UNITY_ANDROID
	private static InterstitialAds instance = null;
	public int totalScore = 0;
	private float adTime = 0;
	public int AdEverySeconds = 8*60;
	public int lastLoadedLevel;

	public int Level0 = 2;
	private bool gameLevel = true;
	private bool enableAds;
	private float lastTime;

	public static InterstitialAds Instance {
		get { return instance; }
	}
	void Awake() {
		if (instance != null && instance != this) {
			Destroy(this.gameObject);
			return;
		} else {
			instance = this;
		}
		DontDestroyOnLoad(this.gameObject);
		adTime = PlayerPrefs.GetInt ("AdTime");
		if (AdEverySeconds - adTime < 150) {
			adTime = 150;
		}
	
	}

	void Start()
	{
		enableAds = GetComponent<StartAppBackPlugin>().enabled;
	}



	void OnLevelWasLoaded(int level) {

		if (lastLoadedLevel == level || !enableAds) return;
		if (this != instance) return;
						
		if (level >= Level0) {
			gameLevel = true;
		} else {
			gameLevel = false;
		}



		lastLoadedLevel = Application.loadedLevel;
		if (gameLevel) {

			adTime += Time.time - lastTime;
			if (adTime > AdEverySeconds) 
			{
				adTime = 0;
				StartAppWrapper.showAd();
				StartAppWrapper.loadAd();
			}
			PlayerPrefs.SetFloat("AdTime",adTime);
			PersistentScript.Instance.SaveData ();
			lastTime = Time.time;
		}
		

	}
	#endif
}
