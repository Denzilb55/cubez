﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Category{Normal = 0, Tricky, Easy}
public class CategoryScript : MenuBehaviour {
	private Category category = Category.Easy;
	private float slideThresh = 38;
	private float switchPause = 0.4f;
	private float switchPeriod;
	public bool isHighlighted = false;
	public GameObject cardTemplate;
	private PersistentScript ps; 
	public List<GameObject> loadedCards = new List<GameObject> ();
	private SelectLevelMenu selectMenu;


	// Use this for initialization
	void Start () {
		selectMenu = GameObject.Find ("Menu").GetComponent<SelectLevelMenu> ();
		textMesh = GetComponent<TextMesh> ();
		fontSize = textMesh.fontSize;
		ps = (GameObject.FindGameObjectWithTag ("Persistent")).GetComponent<PersistentScript>();
		custom_blue = new Color (37f/255f, 24f/255f, 192f/255f);
		custom_red = new Color (210f/255f, 24f/255f, 19f/255f);
		UpdateText ();
		switchPeriod = switchPause;
		LoadCategoryCards (false);
	}
	
	// Update is called once per frame
	void Update () {

		if (switchPeriod < switchPause) {
			switchPeriod += Time.deltaTime;
		} else if (isHighlighted){
			isHighlighted = false;
			//selectMenu.NotifyUnlock();
			Exit();
		}

		if (Input.touchCount > 0) 
		{
			Touch touch = Input.GetTouch (0);
			if (touch.phase == TouchPhase.Moved && switchPeriod >= switchPause) 
			{
				if (touch.deltaPosition.x > slideThresh) 
				{
					NextCategory();	
				
				} 
				else if (touch.deltaPosition.x < -slideThresh) 
				{
					PreviousCategory();	

				}
			}
		}


	}

	public override void Execute()
	{
		base.Execute ();
		NextCategory ();
	}

	private void ClearPreviousCards()
	{
		foreach (GameObject go in loadedCards) {
			Destroy(go);
		}
		loadedCards.Clear ();
	}

	private void LoadCategoryCards(bool doLock)
	{
		if (doLock) {
			selectMenu.NotifyLock ();
		}
		ClearPreviousCards ();
		int xStart = -60;
		int x = xStart;
		int y = 58;
		int z = 47;
		List<LevelScript> levels = ps.levelsCategorized [(int)category];


		int boxesPerLine = 4;
		int maxLines = 2;
		int line = 0;
	
		for (int i = 0; i < levels.Count; i++){
			if (i%boxesPerLine == 0 && i != 0)
			{
				x = xStart;
				y -= 50;
				line++;
				if (line == maxLines)
				{
					break;
				}
			}
			GameObject cardObject = (GameObject)Instantiate (cardTemplate);
			cardObject.transform.position = new Vector3(x,y,z);
			x += 50;
			loadedCards.Add(cardObject);
			LevelCardScript card = cardObject.GetComponent<LevelCardScript> ();
			LevelScript level = ps.levelsCategorized[(int)category][i];
			card.LinkCard (level, i, ps.GetScore(level), ps.IsUnlocked(level)); 

		}
	}

	private void NextCategory()
	{
		if (category == Category.Easy) 
		{
			category = 0;
		} else {
			category++;
		}
		switchPeriod = 0;
		UpdateText ();
		LoadCategoryCards (true);
	}

	private void PreviousCategory()
	{
		if (category == 0) 
		{
			category = Category.Easy;
		} else {
			category--;
		}
		switchPeriod = 0;
		UpdateText ();
		LoadCategoryCards (true);
	}

	void UpdateText()
	{
		textMesh.text = "◀ " +category.ToString () + " ▶";
		HighLight ();
	}

	void HighLight() // same as tickle but lasts longer
	{
		Tickle ();
		isHighlighted = true;
	}
}
