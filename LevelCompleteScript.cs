﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelCompleteScript : MonoBehaviour {

	private GameObject selectedLetterBlock;
	private float oldX;
	private float oldY;
	private float holdPeriod;
	public GameObject scoreText;
	public GameObject retryCube;
	public GameObject unlockTemplate;
	public GameObject continueTemplate;
	public GameObject fireworksParticles;
	public GameObject facebookCube;
	public GameObject twitterCube;
	public GameObject[] cubes;
	float deltaFW;
	float periodFW = 0.2f;
	float periodFWMax = 1.2f;

	GameObject lastFWCube = null;


	 GameObject GetRandomCube()
	{
		int num = UnityEngine.Random.Range(0,cubes.Length);

		return cubes[num];
	}

	public void DoFireworks(bool enableSound)
	{
		GameObject go = GameObject.Instantiate (fireworksParticles) as GameObject;
		GameObject cube = null;
		while (cube == lastFWCube || cube == null) {
			cube = GetRandomCube ();
		}
		lastFWCube = cube;
		//CCube (cube).Glow (0.8f);
		VictoryBlockScript cubeScript = cube.GetComponent<VictoryBlockScript> ();

		if (cubeScript != null)
						cubeScript.Glow ();
		go.transform.position =  cube.transform.position;
		if (enableSound) go.audio.Play ();
	}

	void Initialize()
	{
		Vector3 v3Pos =  new Vector3(0.5f, 0.5f, 90f);
		scoreText.transform.position = Camera.main.ViewportToWorldPoint (v3Pos);
		
		cubes = GameObject.FindGameObjectsWithTag ("LetterBlock");

	}


	void OnLevelWasLoaded(int level)
	{
		Initialize ();
		List<LevelScript> unlocked = PersistentScript.Instance.recentlyUnlockedLevels;

		float startX = 1f;
		

		if (unlocked.Count == 1) {
			startX = 0.25f;	
		} else if (unlocked.Count == 2) {
			startX = 0.2f;
		} else if (unlocked.Count == 0) {
			startX = 0.25f;
		}

		float x = startX;
		GameObject retryBox = Instantiate (retryCube) as GameObject;
		Vector3 v3Pos =  new Vector3(x, 0.2f, 10f);
		retryBox.transform.position = Camera.main.ViewportToWorldPoint (v3Pos);
		x += 0.15f;
		GameObject facebookBox = Instantiate (facebookCube) as GameObject;
		v3Pos =  new Vector3(x, 0.2f, 10f);
		facebookBox.transform.position = Camera.main.ViewportToWorldPoint (v3Pos);
		GameObject twitterBox = Instantiate (twitterCube) as GameObject;
		x += 0.15f;
		v3Pos =  new Vector3(x, 0.2f, 10f);
		twitterBox.transform.position = Camera.main.ViewportToWorldPoint (v3Pos);

		//PlayerPrefs.SetInt ("Completed" + currentIndex, score);
		if (PersistentScript.Instance.highScore) {
			scoreText.GetComponent<TextMesh>().text = "New High Score: " + PersistentScript.Instance.victoryScore;
			if (PersistentScript.Instance.effectsOn)
			{
				DoFireworks(PersistentScript.Instance.SoundEnabled);
			}
		} else {
			scoreText.GetComponent<TextMesh>().text = "Score: " + PersistentScript.Instance.victoryScore;
		}

		if (unlocked.Count == 0) {

			LevelScript next;
			if (PersistentScript.Instance.loadMode == LoadMode.Category)
			{
				next = PersistentScript.Instance.FindNextInCatScript();
			}
			else
			{
				next = PersistentScript.Instance.FindNextInUncatScript();
			}

			if (next != null)
			{
				x += 0.15f;
				GameObject unlockBox = Instantiate (continueTemplate) as GameObject;
				v3Pos =  new Vector3(x, 0.2f, 10f);
				unlockBox.transform.position = Camera.main.ViewportToWorldPoint (v3Pos);
				(unlockBox.GetComponent<UnlockBoxScript>()).FillBox(next);
			}
		} else foreach (LevelScript u in unlocked) {
			x += 0.15f;
			GameObject unlockBox = Instantiate (unlockTemplate) as GameObject;
			v3Pos =  new Vector3(x, 0.2f, 10f);
			unlockBox.transform.position = Camera.main.ViewportToWorldPoint (v3Pos);
			(unlockBox.GetComponent<UnlockBoxScript>()).FillBox(u);
		}
		unlocked.Clear ();
	}
	
	// Update is called once per frame
	void Update () {
		bool holdDown = false;
		bool tapUp = false;
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;

		if (PersistentScript.Instance.themeDark) {
			Camera.main.backgroundColor = new Color (13 / 255f, 13 / 255f, 38 / 255f);
		} else {
			Camera.main.backgroundColor = new Color (60 / 255f, 171 / 255f, 41 / 255f);
		}

		if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor) {
			if (Input.GetMouseButton(0))
			{
				holdDown = true;
			}
			else if (Input.GetMouseButtonUp(0))
			{
				tapUp = true;
			}
		}

		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
			if (Input.touchCount == 1) {
				if (Input.GetMouseButton (0)) {
					holdDown = true;
				}
				else if (Input.GetMouseButtonUp(0))
				{
					tapUp = true;
				}
			}
		}

		if (tapUp) {

			
			if (Physics.Raycast (ray, out hit, 50)) {
				if (hit.transform.tag == "Home")
				{
					Application.LoadLevel(0);
				}
				else if (hit.transform.tag == "LevelCard")
				{
					hit.transform.gameObject.GetComponent<UnlockBoxScript>().Activate();
				}
				else if (hit.transform.tag == "RetryCube")
				{
					PersistentScript.Instance.ReloadCurrentLevel();
				}
				else if (hit.transform.tag == "Social")
				{
					hit.transform.gameObject.GetComponent<SocialBehaviour>().Execute();
				}
			}
		}

		if (holdDown) {
			if (Physics.Raycast (ray, out hit, 50)) {
				if (hit.transform.tag == "LetterBlock")
				{
					selectedLetterBlock = hit.transform.gameObject;
					holdPeriod = 0;
				}
			}

			if (selectedLetterBlock != null) {
				float deltaX = 0;
				float deltaY = 0;
				if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {

						if (Input.touchCount > 0) {
								Touch touch = Input.GetTouch (0);

								if (touch.phase == TouchPhase.Moved) {
										deltaX = touch.deltaPosition.x;
										deltaY = touch.deltaPosition.y;

								}
						}
				}

				if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor) {
						deltaX = Input.mousePosition.x - oldX;
						deltaY = Input.mousePosition.y - oldY;
	

				}



				Vector3 forceVec = new Vector3 (deltaX, 0, deltaY);
				forceVec = Vector3.ClampMagnitude (forceVec * 15f, 300);

				selectedLetterBlock.transform.rigidbody.AddForce (forceVec);



				holdPeriod += Time.deltaTime;

				if (holdPeriod > 0.6f) {
					selectedLetterBlock = null;
				}
			}
		} else {
			selectedLetterBlock = null;
		}
	
		oldX = Input.mousePosition.x;
		oldY = Input.mousePosition.y;
		
		if (PersistentScript.Instance.effectsOn && PersistentScript.Instance.highScore) 
		{
			
			deltaFW += Time.deltaTime;
		//	victoryPeriod += Time.deltaTime;
			if (deltaFW >= periodFW)
			{
				if (Random.Range(0,20) == 1)
				{
					//DoFireworks(victoryPeriod < victorySoundPeriod);
					DoFireworks(PersistentScript.Instance.SoundEnabled);
					deltaFW = 0;
				}
			}
			else if (deltaFW >= periodFWMax)
			{
				DoFireworks(PersistentScript.Instance.SoundEnabled);
				deltaFW = 0;
			}
		}


	}
}
