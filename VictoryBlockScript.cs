﻿using UnityEngine;
using System.Collections;

public class VictoryBlockScript : MonoBehaviour {

	private float GlowPeriod = 1.6f;
	private float currentGlow = 0f;
	private GameObject glowObject;
	public GameObject HaloObject;
	private Light haloLight;
	private float initialGlowRadius = 3f;
	private float finalGlowRadius = 4f;
	public Color GlowColor;


	// Update is called once per frame
	void Update () {
		if (glowObject != null) {
			currentGlow += Time.deltaTime;
			
			float timeFraction = currentGlow/GlowPeriod;
			
			float glowUp = (timeFraction*3f);
			if (glowUp >= 1)
			{
				glowUp = 1;
			}
			
			if (timeFraction > 0.5)
			{
				float alpha = 1 - timeFraction*2 + 0.5f;
				alpha = alpha > 1?1:alpha;
				haloLight.color = new Color(haloLight.color.r,haloLight.color.g,haloLight.color.b,alpha);
			}
			
			haloLight.range = initialGlowRadius + (finalGlowRadius-initialGlowRadius)*glowUp;
			
			
			if (currentGlow > GlowPeriod)
			{
				StopGlow();
			}
		}
	}
	

	
	public void StopGlow()
	{
		if (glowObject != null) {
			Destroy(glowObject);
			glowObject = null;
			haloLight = null;
		}
		
	}
	
	public void Glow()
	{
		if (glowObject == null) {
			glowObject = GameObject.Instantiate (HaloObject, transform.position, transform.rotation) as GameObject;
			glowObject.transform.parent = this.gameObject.transform;
			glowObject.transform.Translate (new Vector3 (0, 0f, 0), transform);
			haloLight = glowObject.GetComponent<Light> ();
			haloLight.range = initialGlowRadius;
		}
		haloLight.color = GlowColor;
		currentGlow = 0f;
	}
	

	
	public void Glow(float period)
	{
		GlowPeriod = period;
		Glow ();
	}
	

}
